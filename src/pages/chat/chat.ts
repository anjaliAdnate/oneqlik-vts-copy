import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Content } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import * as io from 'socket.io-client';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  @ViewChild(Content) content: Content;
  islogin: any;
  fromDate: any;
  toDate: string;
  paramData: any;
  userName: string;
  User: any;
  toUser: any;
  _io: any;
  loader: boolean;
  msgList: any = [];
  user_input: string;
  start_typing: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private apiCall: ApiServiceProvider,
    private loaderController: LoadingController,
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.fromDate = moment({ hours: 0 }).format();
    this.toDate = moment().format();
 
    if(navParams.get("isCustomer")) {
      if (this.navParams.get('params')) {
        this.paramData = this.navParams.get('params').Dealer_ID;
        this.userName = this.paramData.first_name;
        console.log("param data: ", this.paramData);
        this.User = this.islogin._id;
        this.toUser = this.paramData._id;
      }
    } else {
      if (this.navParams.get('params')) {
        this.paramData = this.navParams.get('params');
        this.userName = this.paramData.first_name;
        console.log("param data: ", this.paramData);
        this.User = this.islogin._id;
        this.toUser = this.paramData._id;
      }
    }
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
  }
  userTyping(event: any) {
    debugger
    console.log(event);
    this.start_typing = event.target.value;
    this.scrollDown()
  }
  ngAfterViewInit() {
    this.content.scrollToBottom();
    // This element never changes.
    // let ionapp = document.getElementsByTagName("ion-app")[0];

    // window.addEventListener('keyboardDidShow', async (event) => {
    //   // Move ion-app up, to give room for keyboard
    //   let kbHeight: number = event["keyboardHeight"];
    //   this.kbHeight = kbHeight;
    // });
    // window.addEventListener('keyboardWillHide', () => {
    //   // this.keyboard.show();
    //   // Describe your logic which will be run each time when keyboard is about to be closed.
    // });
  }

  ngOnInit() {
    this.openChatSocket();
    this.getChatHistory();
    // this.innerWidth = window.innerWidth;
    // console.log("window test: ", this.innerWidth)
  }
  openChatSocket() {
    this._io = io('https://www.oneqlik.in/userChat', {
      transports: ['websocket']
    });
    this._io.on('connect', (data) => {
      console.log("userChat connect data: ", data);
    });
    let that = this;
    that._io.on(that.User + "-" + that.toUser, (d4) => {
      if (d4 != undefined)
        (function (data) {
          if (data == undefined) {
            return;
          }
          that.loader = true;
          setTimeout(() => {
            that.msgList.push({
              userId: that.User,
              userName: that.User,
              // userAvatar: "../../assets/chat/chat5.jpg",
              time: new Date().toISOString(),
              message: data
            });
            that.loader = false;
            that.scrollDown();
          }, 2000)
          that.scrollDown();
        })(d4)
    })
  }

  getChatHistory() {
    let that = this;
    // this.loaderController.create({
    //   message: 'please wait we are fetching history...'
    // }).then((loadEl) => {
    //   loadEl.present();
    this.apiCall.startLoading().present();
    var url = this.apiCall.mainUrl + "broadcastNotification/getchatmsg?from=" + this.islogin._id + "&to=" + that.paramData._id;
    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(respData => {
        this.apiCall.stopLoading();
        if (respData) {
          var res = JSON.parse(JSON.stringify(respData));
          for (var i = 0; i < res.length; i++) {
            if (res[i].sender === this.toUser) {
              this.msgList.push({
                userId: this.User,
                userName: this.User,
                time: res[i].timestamp,
                message: res[i].message,
                id: this.msgList.length + 1
              })
            } else {
              if (res[i].sender === this.User) {
                this.msgList.push({
                  userId: this.toUser,
                  userName: this.toUser,
                  time: res[i].timestamp,
                  message: res[i].message,
                  id: this.msgList.length + 1
                })
              }
            }
          }
          setTimeout(() => {
            this.content.scrollToBottom(100);
          }, 50);

          // this.scrollDown();
        }
      },
        err => {
          this.apiCall.stopLoading();
          console.log("chat err: ", err)
        });
    // })

  }
  sendMsg() {
    if (this.user_input !== '') {
      this.msgList.push({
        userId: this.toUser,
        userName: this.toUser,
        time: new Date().toISOString(),
        message: this.user_input,
        id: this.msgList.length + 1
      })
      let that = this;
      that._io.emit('send', this.User, this.toUser, this.user_input);   // three parameters, from(who is sending msg), to(to whom ur sending msg), msg(message string)

      this.user_input = "";
      // setTimeout(() => {
      //   this.content.scrollToBottom(100);
      // }, 50);
      // this.scrollDown();
      // setTimeout(() => {
      //   this.senderSends()
      // }, 500);
    }
  }

  scrollDown() {
    setTimeout(() => {
      // console.log("window height: ", this.innerWidth);
      // console.log("keyboard height: ", this.kbHeight);
      // // this.content.scrollToBottom(this.innerWidth)
      this.content.scrollTo(1500, 1500, 500).then((res) => {
        console.log("scrollToPoint: ", res);
      })
    }, 50);
  }

}
