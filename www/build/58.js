webpackJsonp([58],{

/***/ 545:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__chat__ = __webpack_require__(626);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ChatPageModule = /** @class */ (function () {
    function ChatPageModule() {
    }
    ChatPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__chat__["a" /* ChatPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__chat__["a" /* ChatPage */]),
            ],
        })
    ], ChatPageModule);
    return ChatPageModule;
}());

//# sourceMappingURL=chat.module.js.map

/***/ }),

/***/ 626:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_socket_io_client__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChatPage = /** @class */ (function () {
    function ChatPage(navCtrl, navParams, apiCall, loaderController) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.loaderController = loaderController;
        this.msgList = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.fromDate = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.toDate = __WEBPACK_IMPORTED_MODULE_3_moment__().format();
        if (navParams.get("isCustomer")) {
            if (this.navParams.get('params')) {
                this.paramData = this.navParams.get('params').Dealer_ID;
                this.userName = this.paramData.first_name;
                console.log("param data: ", this.paramData);
                this.User = this.islogin._id;
                this.toUser = this.paramData._id;
            }
        }
        else {
            if (this.navParams.get('params')) {
                this.paramData = this.navParams.get('params');
                this.userName = this.paramData.first_name;
                console.log("param data: ", this.paramData);
                this.User = this.islogin._id;
                this.toUser = this.paramData._id;
            }
        }
    }
    ChatPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ChatPage');
    };
    ChatPage.prototype.userTyping = function (event) {
        debugger;
        console.log(event);
        this.start_typing = event.target.value;
        this.scrollDown();
    };
    ChatPage.prototype.ngAfterViewInit = function () {
        this.content.scrollToBottom();
        // This element never changes.
        // let ionapp = document.getElementsByTagName("ion-app")[0];
        // window.addEventListener('keyboardDidShow', async (event) => {
        //   // Move ion-app up, to give room for keyboard
        //   let kbHeight: number = event["keyboardHeight"];
        //   this.kbHeight = kbHeight;
        // });
        // window.addEventListener('keyboardWillHide', () => {
        //   // this.keyboard.show();
        //   // Describe your logic which will be run each time when keyboard is about to be closed.
        // });
    };
    ChatPage.prototype.ngOnInit = function () {
        this.openChatSocket();
        this.getChatHistory();
        // this.innerWidth = window.innerWidth;
        // console.log("window test: ", this.innerWidth)
    };
    ChatPage.prototype.openChatSocket = function () {
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__('https://www.oneqlik.in/userChat', {
            transports: ['websocket']
        });
        this._io.on('connect', function (data) {
            console.log("userChat connect data: ", data);
        });
        var that = this;
        that._io.on(that.User + "-" + that.toUser, function (d4) {
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    that.loader = true;
                    setTimeout(function () {
                        that.msgList.push({
                            userId: that.User,
                            userName: that.User,
                            // userAvatar: "../../assets/chat/chat5.jpg",
                            time: new Date().toISOString(),
                            message: data
                        });
                        that.loader = false;
                        that.scrollDown();
                    }, 2000);
                    that.scrollDown();
                })(d4);
        });
    };
    ChatPage.prototype.getChatHistory = function () {
        var _this = this;
        var that = this;
        // this.loaderController.create({
        //   message: 'please wait we are fetching history...'
        // }).then((loadEl) => {
        //   loadEl.present();
        this.apiCall.startLoading().present();
        var url = this.apiCall.mainUrl + "broadcastNotification/getchatmsg?from=" + this.islogin._id + "&to=" + that.paramData._id;
        this.apiCall.getdevicesForAllVehiclesApi(url)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            if (respData) {
                var res = JSON.parse(JSON.stringify(respData));
                for (var i = 0; i < res.length; i++) {
                    if (res[i].sender === _this.toUser) {
                        _this.msgList.push({
                            userId: _this.User,
                            userName: _this.User,
                            time: res[i].timestamp,
                            message: res[i].message,
                            id: _this.msgList.length + 1
                        });
                    }
                    else {
                        if (res[i].sender === _this.User) {
                            _this.msgList.push({
                                userId: _this.toUser,
                                userName: _this.toUser,
                                time: res[i].timestamp,
                                message: res[i].message,
                                id: _this.msgList.length + 1
                            });
                        }
                    }
                }
                setTimeout(function () {
                    _this.content.scrollToBottom(100);
                }, 50);
                // this.scrollDown();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("chat err: ", err);
        });
        // })
    };
    ChatPage.prototype.sendMsg = function () {
        if (this.user_input !== '') {
            this.msgList.push({
                userId: this.toUser,
                userName: this.toUser,
                time: new Date().toISOString(),
                message: this.user_input,
                id: this.msgList.length + 1
            });
            var that = this;
            that._io.emit('send', this.User, this.toUser, this.user_input); // three parameters, from(who is sending msg), to(to whom ur sending msg), msg(message string)
            this.user_input = "";
            // setTimeout(() => {
            //   this.content.scrollToBottom(100);
            // }, 50);
            // this.scrollDown();
            // setTimeout(() => {
            //   this.senderSends()
            // }, 500);
        }
    };
    ChatPage.prototype.scrollDown = function () {
        var _this = this;
        setTimeout(function () {
            // console.log("window height: ", this.innerWidth);
            // console.log("keyboard height: ", this.kbHeight);
            // // this.content.scrollToBottom(this.innerWidth)
            _this.content.scrollTo(1500, 1500, 500).then(function (res) {
                console.log("scrollToPoint: ", res);
            });
        }, 50);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Content"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Content"])
    ], ChatPage.prototype, "content", void 0);
    ChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-chat',template:/*ion-inline-start:"/Users/apple/Desktop/oneqlik-projects/oneqlik-vts-updated/src/pages/chat/chat.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{userName}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content #IonContent fullscreen="false">\n  <!-- <div class="messages-container"> -->\n  <ion-list>\n    <div *ngFor="let chat of msgList; let i = index; ">\n      <ion-row *ngIf="chat.userId == User">\n        <ion-col class="right" no-padding\n          [ngClass]="{\'clubbed\':((msgList[i+1] && msgList[i+1].userId != chat.userId)|| !msgList[i+1])}">\n          <div class="imageAvatarRight">\n            <!-- <div class="imageAvatarBottom">\n              <ion-avatar class="avatar" [ngClass]="(msgList[i+1] && msgList[i+1].userId == chat.userId)?\'hidden\':\'\'">\n                <div class="imageAvatarBottomIcon">\n                  <ion-icon name="add" expand="icon-only" color="light"> </ion-icon>\n                </div>\n                <img src="assets/chat1.jpg" />\n              </ion-avatar>\n            </div> -->\n            <ion-label color="light" style="margin: 0px;">\n              <div class="chatDiv" [ngClass]="{\'sharper\':((msgList[i+1] && msgList[i+1].userId == chat.userId) && \n              (msgList[i-1] && msgList[i-1].userId == chat.userId)),\n              \'sharper-top\':(msgList[i-1] && msgList[i-1].userId == chat.userId),\n              \'sharper-bottom\':(msgList[i+1] && msgList[i+1].userId == chat.userId)}">\n                <p text-wrap padding>{{chat.message}}\n                </p>\n                <div class="corner-parent-right">\n                  <div class="corner-child-right">\n\n                  </div>\n                </div>\n              </div>\n            </ion-label>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf="chat.userId == toUser">\n        <ion-col class="left" no-padding\n          [ngClass]="{\'clubbed\':((msgList[i+1] && msgList[i+1].userId != chat.userId)|| !msgList[i+1])}">\n          <div class="imageAvatarLeft">\n            <ion-label color="light" style="margin: 0px;">\n              <div class="chatDiv" [ngClass]="{\'sharper\':((msgList[i+1] && msgList[i+1].userId == chat.userId) && \n              (msgList[i-1] && msgList[i-1].userId == chat.userId)),\n              \'sharper-top\':(msgList[i-1] && msgList[i-1].userId == chat.userId),\n              \'sharper-bottom\':(msgList[i+1] && msgList[i+1].userId == chat.userId)}">\n                <p text-wrap padding>{{chat.message}}</p>\n                <div class="corner-parent-left">\n                  <div class="corner-child-left">\n\n                  </div>\n                </div>\n              </div>\n            </ion-label>\n            <!-- <div class="imageAvatarBottom">\n\n              <ion-avatar class="avatar" [ngClass]="(msgList[i+1] && msgList[i+1].userId == chat.userId)?\'hidden\':\'\'">\n                <div class="imageAvatarBottomIcon">\n                  <ion-icon name="add" expand="icon-only" color="light"> </ion-icon>\n                </div>\n                <img src="assets/chat6.jpg" />\n              </ion-avatar>\n            </div> -->\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf="((msgList[i+1] && msgList[i+1].userId != chat.userId)|| !msgList[i+1])">\n        <ion-col>\n          <p>{{chat.time | date:\'shortTime\'}}</p>\n\n          <!-- <ion-text>{{chat.time | date:\'shortTime\'}}</ion-text> -->\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-list>\n\n  <!-- <ion-fab vertical="bottom" horizontal="end" edge slot="fixed">\n    <ion-fab-button (click)="sendMsg()" color="secondary">\n      <ion-icon name="send" expand="icon-only"></ion-icon>\n    </ion-fab-button>\n  </ion-fab> -->\n\n  <ion-row *ngIf="loader">\n    <ion-col no-padding class="loading-col">\n      <div class="imageAvatarRight">\n        <!-- <div class="imageAvatarBottomLoader">\n          <ion-avatar class="avatar">\n            <div class="imageAvatarBottomIcon">\n              <ion-icon name="add" expand="icon-only" color="light"> </ion-icon>\n            </div>\n            <img src="assets/chat6.jpg" />\n          </ion-avatar>\n        </div> -->\n        <ion-label>\n          <div class="chatDivLoader">\n            <ion-spinner name="dots" color="light"></ion-spinner>\n            <!-- <ion-img src="../../assets/chat/loader.gif"></ion-img> -->\n            <!-- <p text-wrap padding> {{paramData.name || \'Pam\'}} is typing...</p> -->\n            <div class="corner-parent-right">\n              <div class="corner-child-right">\n\n              </div>\n            </div>\n          </div>\n        </ion-label>\n      </div>\n    </ion-col>\n  </ion-row>\n  <!-- </div> -->\n</ion-content>\n<ion-footer>\n  <!-- <ion-item> -->\n    <ion-row>\n      <ion-col col-10>\n        <ion-input type="text" placeholder="Type a message" [(ngModel)]="user_input"></ion-input>\n      </ion-col>\n      <ion-col col-2>\n        <button ion-fab mini (click)="sendMsg()" color="secondary" end>\n          <ion-icon name="send" icon-only></ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n    <!-- <ion-icon slot="start" name="camera" expand="icon-only" class="footerIcon"></ion-icon> -->\n    <!-- <ion-textarea autoGrow autofocus placeholder="Write a message..." rows="1" [(ngModel)]="user_input"\n      (ionFocus)="userTyping($event)"></ion-textarea> -->\n\n    <!-- <textarea #myInput id="myInput" placeholder="Write a message..." rows="1" maxLength="500"  [(ngModel)]="user_input" (ionFocus)="userTyping($event)"></textarea> -->\n    <!-- <ion-fab slot="end"> -->\n\n    <!-- </ion-fab> -->\n  <!-- </ion-item> -->\n</ion-footer>'/*ion-inline-end:"/Users/apple/Desktop/oneqlik-projects/oneqlik-vts-updated/src/pages/chat/chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"]])
    ], ChatPage);
    return ChatPage;
}());

//# sourceMappingURL=chat.js.map

/***/ })

});
//# sourceMappingURL=58.js.map